package com.github.steveice10.mc.protocol.data.game.world.particle;

import com.github.steveice10.mc.protocol.data.game.world.block.BlockState;
import com.github.steveice10.mc.protocol.data.game.world.block.state.IBlockState;
import com.github.steveice10.mc.protocol.util.ObjectUtil;

import java.util.Objects;

public class BlockParticleData implements ParticleData {
    private final IBlockState blockState;

    public BlockParticleData(IBlockState blockState) {
        this.blockState = blockState;
    }

    public IBlockState getBlockState() {
        return this.blockState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof BlockParticleData)) return false;

        BlockParticleData that = (BlockParticleData) o;
        return Objects.equals(this.blockState, that.blockState);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(this.blockState);
    }

    @Override
    public String toString() {
        return ObjectUtil.toString(this);
    }
}
