package com.github.steveice10.mc.protocol.data.game.world.block.property;

/**
 * @author elmexl
 * Created on 28.05.2019.
 */
public interface IBlockPropertyEnum {
    public String getName();
}
