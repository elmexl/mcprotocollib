package com.github.steveice10.mc.protocol.data.game.world.block.registry;

/**
 * @author elmexl
 * Created on 28.05.2019.
 */
public interface IRegistry<T> extends Iterable<T> {
    int getRegistryIndex(T t);

    T getRegistryEntryByIndex(int index);

    void register(T entry);

    void register(int index, T entry);
}
