package com.github.steveice10.mc.protocol.data.game.world.effect;

import com.github.steveice10.mc.protocol.data.game.world.block.BlockState;
import com.github.steveice10.mc.protocol.data.game.world.block.state.IBlockState;
import com.github.steveice10.mc.protocol.util.ObjectUtil;

import java.util.Objects;

public class BreakBlockEffectData implements WorldEffectData {
    private IBlockState blockState;

    public BreakBlockEffectData(IBlockState blockState) {
        this.blockState = blockState;
    }

    public IBlockState getBlockState() {
        return this.blockState;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(!(o instanceof BreakBlockEffectData)) return false;

        BreakBlockEffectData that = (BreakBlockEffectData) o;
        return Objects.equals(this.blockState, that.blockState);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(this.blockState);
    }

    @Override
    public String toString() {
        return ObjectUtil.toString(this);
    }
}
