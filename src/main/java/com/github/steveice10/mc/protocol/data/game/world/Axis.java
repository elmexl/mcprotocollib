package com.github.steveice10.mc.protocol.data.game.world;

import com.github.steveice10.mc.protocol.data.game.world.block.property.IBlockPropertyEnum;

public enum Axis implements IBlockPropertyEnum {
    X("x"),
    Y("y"),
    Z("z"),
    NONE("none");

    private final String name;

    Axis(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
