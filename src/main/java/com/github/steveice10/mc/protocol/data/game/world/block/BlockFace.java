package com.github.steveice10.mc.protocol.data.game.world.block;

import com.github.steveice10.mc.protocol.data.game.world.block.property.IBlockPropertyEnum;

public enum BlockFace implements IBlockPropertyEnum {
    DOWN,
    UP,
    NORTH,
    SOUTH,
    WEST,
    EAST,
    SPECIAL;

    @Override
    public String getName() {
        return this.name().toLowerCase();
    }
}
