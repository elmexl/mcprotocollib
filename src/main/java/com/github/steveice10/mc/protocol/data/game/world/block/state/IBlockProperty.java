package com.github.steveice10.mc.protocol.data.game.world.block.state;

import java.util.Collection;
import java.util.Optional;

/**
 * @author elmexl
 * Created on 28.05.2019.
 */
public interface IBlockProperty <T extends Comparable<T>> {
    String getName();

    Collection<T> getAllowedValues();

    Optional<T> parseValue(String value);
}
