package com.github.steveice10.mc.protocol.data.game.world.block;

import com.github.steveice10.mc.protocol.data.game.world.block.registry.RegistryArrayImpl;
import com.github.steveice10.mc.protocol.data.game.world.block.state.IBlockState;

import javax.annotation.Nonnull;

/**
 * @author elmexl
 * Created on 26.05.2019.
 */
public class BlockStateRegistry extends RegistryArrayImpl<IBlockState> {

    private final static int MAX_BLOCK_DATA = 11270 + 1;


    public final static BlockStateRegistry INSTANCE = new BlockStateRegistry();

    private IBlockState AIR;


    private BlockStateRegistry() {
        super(MAX_BLOCK_DATA);
    }

    @Override
    @Nonnull
    public IBlockState getRegistryEntryByIndex(int index) {
        IBlockState ret = super.getRegistryEntryByIndex(index);
        if (ret == null)
            return AIR;
        return ret;
    }

    @Override
    public void register(int index, IBlockState entry) {
        super.register(index, entry);
        if (index == 0) {
            this.AIR = entry;
        }
    }
}
