package com.github.steveice10.mc.protocol.data.game.world.block.state;

import com.github.steveice10.mc.protocol.data.game.world.block.Block;
import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Map;

/**
 * @author elmexl
 * Created on 27.05.2019.
 */
public interface IBlockState {

    Collection<IBlockProperty<?>> getPropertyKeys();

    <T extends Comparable<T>> T getProperty(IBlockProperty<T> property);

    ImmutableMap<IBlockProperty<?>, Comparable<?>> getProperties();

    Block getBlock();

    int getGlobalPaletteIndex();

    <T extends Comparable<T>> IBlockState getWithProperty(IBlockProperty<T> property, T val);
}
